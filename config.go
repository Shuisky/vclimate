package main

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
)

const defaultConfigPath = "/.vclimate/config.json"

// Config for application
type Config struct {
	Host      string
	IgnoreSSL bool
	Username  string
	Password  string
}

// LoadConfig parses config file
func LoadConfig() Config {
	home, err := os.UserHomeDir()
	file, err := os.Open(home + filepath.FromSlash(defaultConfigPath))
	if err != nil {
		fmt.Printf("Error while loading config: %s\n", err)
		os.Exit(2)
	}
	defer file.Close()
	decoder := json.NewDecoder(file)
	configuration := Config{}
	err = decoder.Decode(&configuration)
	if err != nil {
		fmt.Println("error:", err)
	}
	return configuration
}
