package main

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

// Session json data
type Session struct {
	Value string
}

func getSession() string {
	res := requestSession("POST", "/rest/com/vmware/cis/session")
	// var result map[string]string
	// json.Unmarshal([]byte(res), &result)
	// return result["value"]
	var session Session
	json.Unmarshal([]byte(res), &session)
	return session.Value
}

func requestSession(method string, path string) string {
	config := LoadConfig()
	if config.IgnoreSSL {
		http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	}
	req, _ := http.NewRequest(method, config.Host+path, nil)
	req.SetBasicAuth(config.Username, config.Password)
	response, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Printf("Can't connect to %s\n", config.Host)
		os.Exit(2)
	}
	data, _ := ioutil.ReadAll(response.Body)
	return string(data)
}
