package main

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/TylerBrock/colorjson"
)

// RequestAPI function to make standard request to vcenter
func RequestAPI(method string, path string, session string) []byte {
	config := LoadConfig()
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	req, _ := http.NewRequest(method, config.Host+path, nil)
	req.Header.Set("vmware-api-session-id", session)
	response, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Printf("Can't connect to %s\n", config.Host)
		os.Exit(2)
	}
	data, _ := ioutil.ReadAll(response.Body)
	return data
}

func formatJSON(data []byte) string {
	var obj map[string]interface{}
	json.Unmarshal([]byte(string(data)), &obj)

	f := colorjson.NewFormatter()
	f.Indent = 4

	s, _ := f.Marshal(obj)
	return string(s)
}
