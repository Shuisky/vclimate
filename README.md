**vclimate** - v(for vSphere) cli(command line interface) mate(buddy)

Tested on vSphere version 6.7

Available commands:
    version - to get app version
    help - to print help
    get - to get data from server
        vms - get virtual machines list
        vm name - get virtual machine info
        rps - get resource pools list
    
    --json - format output in json

Basic config should be in *home_dir/.vclimate/config.json*
```
{
    "host": "https://hostname",
    "ignoreSSL": false,
    "username": "",
    "password": ""
}
```