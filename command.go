package main

import (
	"fmt"
	"os"
)

func getArgs() []string {
	return os.Args[1:]
}

func isArgsContains(slice []string, val string) bool {
	for _, item := range slice {
		if item == val {
			return true
		}
	}
	return false
}

func processMainCommand() {
	args := getArgs()
	if len(args) > 0 {
		switch args[0] {
		case "help":
			printHelp()
		case "get":
			processGetCommand(args[1:])
		case "version":
			fmt.Println("vclimate version: " + appVersion)
		default:
			fmt.Println("Unknown command")
			printHelp()
		}
	} else {
		printHelp()
	}
}

func processGetCommand(args []string) {
	if len(args) > 0 {
		switch args[0] {
		case "vms":
			vml := VirtualMachineList{}
			vml.getVMs(getSession())
			if isArgsContains(args, "--json") {
				fmt.Println(vml.JSON)
			} else {
				vml.printVMsTable()
			}
		case "vm":
			processGetVMCommand(args[1:])
		case "rps":
			rpl := ResourcePoolList{}
			rpl.getRPs(getSession())
			if isArgsContains(args, "--json") {
				fmt.Println(rpl.JSON)
			} else {
				rpl.printRPsTable()
			}
		default:
			fmt.Println("Unknown command")
			printGetHelp()
		}
	} else {
		printGetHelp()
	}
}

func processGetVMCommand(args []string) {
	if len(args) > 0 {
		vm := VirtualMachine{}
		vm.getVM(getSession(), args[0])
		if isArgsContains(args, "--json") {
			fmt.Println(vm.JSON)
		} else {
			vm.printVMInfo()
		}
	} else {
		printGetVMHelp()
	}
}
