package main

import "fmt"

func printHelp() {
	fmt.Println("Usage:")
	fmt.Println("help - print this text")
	fmt.Println("version - print vclimate version")
	fmt.Println("get - get info about vcenter objects")
}

func printGetHelp() {
	fmt.Println("Available 'get' commands:")
	fmt.Println("vms - get virtual machines list")
	fmt.Println("vm - get virtual machine info")
	fmt.Println("rps - get resource pools list")
	fmt.Println("Available parameters:")
	fmt.Println("--json - print output in json format")
	fmt.Println("Example 1: vclimate get vms")
	fmt.Println("Example 2: vclimate get vm name --json")
}

func printGetVMHelp() {
	fmt.Println("Example: get vm name-of-vm")
}
