package main

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/olekukonko/tablewriter"
)

// ResourcePoolList json data
type ResourcePoolList struct {
	List []struct {
		Name         string `json:"name"`
		ResourcePool string `json:"resource_pool"`
	} `json:"value"`
	JSON string
}

func (rpl *ResourcePoolList) getRPs(session string) {
	path := "/rest/vcenter/resource-pool"
	res := RequestAPI("GET", path, session)
	rpl.JSON = string(res)
	json.Unmarshal(res, &rpl)
}

func (rpl ResourcePoolList) printRPsTable() {
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"#", "Name", "Resource pool"})
	var row []string
	for i, v := range rpl.List {
		row = []string{fmt.Sprint(i + 1), fmt.Sprint(v.Name), fmt.Sprint(v.ResourcePool)}
		table.Append(row)
	}
	table.Render()
}
