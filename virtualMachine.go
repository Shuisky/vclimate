package main

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"

	"github.com/olekukonko/tablewriter"
)

// VirtualMachineList json data
type VirtualMachineList struct {
	List []struct {
		MemorySizeMiB uint   `json:"memory_size_MiB"`
		VM            string `json:"vm"`
		Name          string `json:"name"`
		PowerState    string `json:"power_state"`
		CPUCount      uint   `json:"cpu_count"`
	} `json:"value"`
	JSON string
}

func (vml *VirtualMachineList) getVMs(session string) {
	path := "/rest/vcenter/vm"
	res := RequestAPI("GET", path, session)
	vml.JSON = string(res)
	json.Unmarshal(res, &vml)
}

func (vml VirtualMachineList) printVMsTable() {
	fmt.Println("Displaying first 1000 virtual machines")
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"#", "CPU Count", "Mem Size Mib", "Name", "Power State", "VM"})
	var row []string
	for i, v := range vml.List {
		row = []string{fmt.Sprint(i + 1), fmt.Sprint(v.CPUCount), fmt.Sprint(v.MemorySizeMiB), v.Name, v.PowerState, v.VM}
		if v.PowerState == "POWERED_ON" {
			table.Rich(row, []tablewriter.Colors{{}, {}, {}, {}, {tablewriter.Bold, tablewriter.FgGreenColor}, {}})
		} else {
			table.Rich(row, []tablewriter.Colors{{}, {}, {}, {}, {tablewriter.Bold, tablewriter.FgRedColor}, {}})
		}
	}
	table.Render()
}

// VirtualMachine json data
type VirtualMachine struct {
	Info struct {
		Boot struct {
			Delay           int    `json:"delay"`
			EfiLegacyBoot   bool   `json:"efi_legacy_boot"`
			EnterSetupMode  bool   `json:"enter_setup_mode"`
			NetworkProtocol string `json:"network_protocol"`
			Retry           bool   `json:"retry"`
			RetryDelay      int    `json:"retry_delay"`
			Type            string `json:"type"`
		} `json:"boot"`
		BootDevices []struct {
			Disks []string `json:"disks"`
			Nic   string   `json:"nic"`
			Type  string   `json:"type"`
		} `json:"boot_devices"`
		Cdroms []struct {
			Key   string `json:"key"`
			Value struct {
				AllowGuestControl bool `json:"allow_guest_control"`
				Backing           struct {
					AutoDetect       bool   `json:"auto_detect"`
					DeviceAccessType string `json:"device_access_type"`
					HostDevice       string `json:"host_device"`
					IsoFile          string `json:"iso_file"`
					Type             string `json:"type"`
				} `json:"backing"`
				Ide struct {
					Master  bool `json:"master"`
					Primary bool `json:"primary"`
				} `json:"ide"`
				Label string `json:"label"`
				Sata  struct {
					Bus  int `json:"bus"`
					Unit int `json:"unit"`
				} `json:"sata"`
				StartConnected bool   `json:"start_connected"`
				State          string `json:"state"`
				Type           string `json:"type"`
			} `json:"value"`
		} `json:"cdroms"`
		CPU struct {
			CoresPerSocket   int  `json:"cores_per_socket"`
			Count            int  `json:"count"`
			HotAddEnabled    bool `json:"hot_add_enabled"`
			HotRemoveEnabled bool `json:"hot_remove_enabled"`
		} `json:"cpu"`
		Disks []struct {
			Key   string `json:"key"`
			Value struct {
				Backing struct {
					Type     string `json:"type"`
					VmdkFile string `json:"vmdk_file"`
				} `json:"backing"`
				Capacity int `json:"capacity"`
				Ide      struct {
					Master  bool `json:"master"`
					Primary bool `json:"primary"`
				} `json:"ide"`
				Label string `json:"label"`
				Sata  struct {
					Bus  int `json:"bus"`
					Unit int `json:"unit"`
				} `json:"sata"`
				Scsi struct {
					Bus  int `json:"bus"`
					Unit int `json:"unit"`
				} `json:"scsi"`
				Type string `json:"type"`
			} `json:"value"`
		} `json:"disks"`
		Floppies []struct {
			Key   string `json:"key"`
			Value struct {
				AllowGuestControl bool `json:"allow_guest_control"`
				Backing           struct {
					AutoDetect bool   `json:"auto_detect"`
					HostDevice string `json:"host_device"`
					ImageFile  string `json:"image_file"`
					Type       string `json:"type"`
				} `json:"backing"`
				Label          string `json:"label"`
				StartConnected bool   `json:"start_connected"`
				State          string `json:"state"`
			} `json:"value"`
		} `json:"floppies"`
		GuestOS  string `json:"guest_OS"`
		Hardware struct {
			UpgradePolicy  string `json:"upgrade_policy"`
			UpgradeStatus  string `json:"upgrade_status"`
			UpgradeVersion string `json:"upgrade_version"`
			Version        string `json:"version"`
		} `json:"hardware"`
		Identity struct {
			BiosUUID     string `json:"bios_uuid"`
			InstanceUUID string `json:"instance_uuid"`
			Name         string `json:"name"`
		} `json:"identity"`
		InstantCloneFrozen bool `json:"instant_clone_frozen"`
		Memory             struct {
			HotAddEnabled          bool `json:"hot_add_enabled"`
			HotAddIncrementSizeMiB int  `json:"hot_add_increment_size_MiB"`
			HotAddLimitMiB         int  `json:"hot_add_limit_MiB"`
			SizeMiB                int  `json:"size_MiB"`
		} `json:"memory"`
		Name string `json:"name"`
		Nics []struct {
			Key   string `json:"key"`
			Value struct {
				AllowGuestControl bool `json:"allow_guest_control"`
				Backing           struct {
					ConnectionCookie      int    `json:"connection_cookie"`
					DistributedPort       string `json:"distributed_port"`
					DistributedSwitchUUID string `json:"distributed_switch_uuid"`
					HostDevice            string `json:"host_device"`
					Network               string `json:"network"`
					NetworkName           string `json:"network_name"`
					OpaqueNetworkID       string `json:"opaque_network_id"`
					OpaqueNetworkType     string `json:"opaque_network_type"`
					Type                  string `json:"type"`
				} `json:"backing"`
				Label                   string `json:"label"`
				MacAddress              string `json:"mac_address"`
				MacType                 string `json:"mac_type"`
				PciSlotNumber           int    `json:"pci_slot_number"`
				StartConnected          bool   `json:"start_connected"`
				State                   string `json:"state"`
				Type                    string `json:"type"`
				UptCompatibilityEnabled bool   `json:"upt_compatibility_enabled"`
				WakeOnLanEnabled        bool   `json:"wake_on_lan_enabled"`
			} `json:"value"`
		} `json:"nics"`
		ParallelPorts []struct {
			Key   string `json:"key"`
			Value struct {
				AllowGuestControl bool `json:"allow_guest_control"`
				Backing           struct {
					AutoDetect bool   `json:"auto_detect"`
					File       string `json:"file"`
					HostDevice string `json:"host_device"`
					Type       string `json:"type"`
				} `json:"backing"`
				Label          string `json:"label"`
				StartConnected bool   `json:"start_connected"`
				State          string `json:"state"`
			} `json:"value"`
		} `json:"parallel_ports"`
		PowerState   string `json:"power_state"`
		SataAdapters []struct {
			Key   string `json:"key"`
			Value struct {
				Bus           int    `json:"bus"`
				Label         string `json:"label"`
				PciSlotNumber int    `json:"pci_slot_number"`
				Type          string `json:"type"`
			} `json:"value"`
		} `json:"sata_adapters"`
		ScsiAdapters []struct {
			Key   string `json:"key"`
			Value struct {
				Label         string `json:"label"`
				PciSlotNumber int    `json:"pci_slot_number"`
				Scsi          struct {
					Bus  int `json:"bus"`
					Unit int `json:"unit"`
				} `json:"scsi"`
				Sharing string `json:"sharing"`
				Type    string `json:"type"`
			} `json:"value"`
		} `json:"scsi_adapters"`
		SerialPorts []struct {
			Key   string `json:"key"`
			Value struct {
				AllowGuestControl bool `json:"allow_guest_control"`
				Backing           struct {
					AutoDetect      bool   `json:"auto_detect"`
					File            string `json:"file"`
					HostDevice      string `json:"host_device"`
					NetworkLocation string `json:"network_location"`
					NoRxLoss        bool   `json:"no_rx_loss"`
					Pipe            string `json:"pipe"`
					Proxy           string `json:"proxy"`
					Type            string `json:"type"`
				} `json:"backing"`
				Label          string `json:"label"`
				StartConnected bool   `json:"start_connected"`
				State          string `json:"state"`
				YieldOnPoll    bool   `json:"yield_on_poll"`
			} `json:"value"`
		} `json:"serial_ports"`
		Messages []struct {
			Args           []interface{} `json:"args"`
			DefaultMessage string        `json:"default_message"`
			ID             string        `json:"id"`
		} `json:"messages"`
	} `json:"value"`
	Type string `json:"type"`
	JSON string
}

func (vm *VirtualMachine) getVM(session string, vmName string) {
	path := "/rest/vcenter/vm/" + vmName
	res := RequestAPI("GET", path, session)
	vm.JSON = string(res)
	json.Unmarshal(res, &vm)
	if len(vm.Info.Messages) > 0 {
		fmt.Printf("VM '%v' not found\n", vmName)
		for _, v := range vm.Info.Messages {
			fmt.Println("Message: " + v.DefaultMessage)
		}
		os.Exit(1)
	}
}

func (vm VirtualMachine) printVMInfo(indentation ...string) {
	var indent string
	if len(indentation) > 0 {
		indent = indentation[0]
	} else {
		indent = "  "
	}
	vmi := vm.Info
	fmt.Println("Boot:")
	fmt.Printf(indent+"Delay: %v\n", vmi.Boot.Delay)
	fmt.Printf(indent+"EfiLegacyBoot: %v\n", vmi.Boot.EfiLegacyBoot)
	fmt.Printf(indent+"EnterSetupMode: %v\n", vmi.Boot.EnterSetupMode)
	fmt.Printf(indent+"NetworkProtocol: %v\n", vmi.Boot.NetworkProtocol)
	fmt.Printf(indent+"Retry: %v\n", vmi.Boot.Retry)
	fmt.Printf(indent+"RetryDelay: %v\n", vmi.Boot.RetryDelay)
	fmt.Printf(indent+"Type: %v\n", vmi.Boot.Type)
	fmt.Println("Boot Devices:")
	for i, v := range vmi.BootDevices {
		fmt.Printf(indent+"%v:\n", i+1)
		fmt.Printf(strings.Repeat(indent, 2)+"Disks: %v\n", v.Disks)
		fmt.Printf(strings.Repeat(indent, 2)+"Nic: %v\n", v.Nic)
		fmt.Printf(strings.Repeat(indent, 2)+"Type: %v\n", v.Type)
	}
	fmt.Println("Cdroms:")
	for i, v := range vmi.Cdroms {
		fmt.Printf(indent+"%v:\n", i+1)
		fmt.Printf(strings.Repeat(indent, 2)+"Key: %v\n", v.Key)
		fmt.Println(strings.Repeat(indent, 2) + "Value:")
		fmt.Printf(strings.Repeat(indent, 3)+"AllowGuestControl: %v\n", v.Value.AllowGuestControl)
		fmt.Println(strings.Repeat(indent, 3) + "Backing:")
		fmt.Printf(strings.Repeat(indent, 4)+"AutoDetect: %v\n", v.Value.Backing.AutoDetect)
		fmt.Printf(strings.Repeat(indent, 4)+"DeviceAccessType: %v\n", v.Value.Backing.DeviceAccessType)
		fmt.Printf(strings.Repeat(indent, 4)+"HostDevice: %v\n", v.Value.Backing.HostDevice)
		fmt.Printf(strings.Repeat(indent, 4)+"IsoFile: %v\n", v.Value.Backing.IsoFile)
		fmt.Printf(strings.Repeat(indent, 4)+"Type: %v\n", v.Value.Backing.Type)
		fmt.Println(strings.Repeat(indent, 3) + "Ide:")
		fmt.Printf(strings.Repeat(indent, 4)+"Master: %v\n", v.Value.Ide.Master)
		fmt.Printf(strings.Repeat(indent, 4)+"Primary: %v\n", v.Value.Ide.Primary)
		fmt.Printf(strings.Repeat(indent, 3)+"Label: %v\n", v.Value.Label)
		fmt.Println(strings.Repeat(indent, 3) + "Sata:")
		fmt.Printf(strings.Repeat(indent, 4)+"Bus: %v\n", v.Value.Sata.Bus)
		fmt.Printf(strings.Repeat(indent, 4)+"Unit: %v\n", v.Value.Sata.Unit)
		fmt.Printf(strings.Repeat(indent, 3)+"StartConnected: %v\n", v.Value.StartConnected)
		fmt.Printf(strings.Repeat(indent, 3)+"State: %v\n", v.Value.State)
		fmt.Printf(strings.Repeat(indent, 3)+"Type: %v\n", v.Value.Type)
	}
	fmt.Println("CPU:")
	fmt.Printf(indent+"CoresPerSocket: %v\n", vmi.CPU.CoresPerSocket)
	fmt.Printf(indent+"Count: %v\n", vmi.CPU.Count)
	fmt.Printf(indent+"HotAddEnabled: %v\n", vmi.CPU.HotAddEnabled)
	fmt.Printf(indent+"HotRemoveEnabled: %v\n", vmi.CPU.HotRemoveEnabled)
	fmt.Println("Disks:")
	for i, v := range vmi.Disks {
		fmt.Printf(indent+"%v:\n", i+1)
		fmt.Printf(strings.Repeat(indent, 2)+"Key: %v\n", v.Key)
		fmt.Println(strings.Repeat(indent, 2) + "Value:")
		fmt.Println(strings.Repeat(indent, 3) + "Backing:")
		fmt.Printf(strings.Repeat(indent, 4)+"Type: %v\n", v.Value.Backing.Type)
		fmt.Printf(strings.Repeat(indent, 4)+"VmdkFile: %v\n", v.Value.Backing.VmdkFile)
		fmt.Printf(strings.Repeat(indent, 3)+"Capacity: %v\n", v.Value.Capacity)
		fmt.Println(strings.Repeat(indent, 3) + "Ide:")
		fmt.Printf(strings.Repeat(indent, 4)+"Master: %v\n", v.Value.Ide.Master)
		fmt.Printf(strings.Repeat(indent, 4)+"Primary: %v\n", v.Value.Ide.Primary)
		fmt.Printf(strings.Repeat(indent, 3)+"Label: %v\n", v.Value.Label)
		fmt.Println(strings.Repeat(indent, 3) + "Sata:")
		fmt.Printf(strings.Repeat(indent, 4)+"Bus: %v\n", v.Value.Sata.Bus)
		fmt.Printf(strings.Repeat(indent, 4)+"Unit: %v\n", v.Value.Sata.Unit)
		fmt.Println(strings.Repeat(indent, 3) + "Scsi:")
		fmt.Printf(strings.Repeat(indent, 4)+"Bus: %v\n", v.Value.Scsi.Bus)
		fmt.Printf(strings.Repeat(indent, 4)+"Unit: %v\n", v.Value.Scsi.Unit)
		fmt.Printf(strings.Repeat(indent, 3)+"Type: %v\n", v.Value.Type)
	}
	fmt.Println("Floppies:")
	for i, v := range vmi.Floppies {
		fmt.Printf(indent+"%v:\n", i+1)
		fmt.Printf(strings.Repeat(indent, 2)+"Key: %v\n", v.Key)
		fmt.Println(strings.Repeat(indent, 2) + "Value:")
		fmt.Printf(strings.Repeat(indent, 3)+"AllowGuestControl: %v\n", v.Value.AllowGuestControl)
		fmt.Println(strings.Repeat(indent, 3) + "Backing:")
		fmt.Printf(strings.Repeat(indent, 4)+"AutoDetect: %v\n", v.Value.Backing.AutoDetect)
		fmt.Printf(strings.Repeat(indent, 4)+"HostDevice: %v\n", v.Value.Backing.HostDevice)
		fmt.Printf(strings.Repeat(indent, 4)+"ImageFile: %v\n", v.Value.Backing.ImageFile)
		fmt.Printf(strings.Repeat(indent, 4)+"Type: %v\n", v.Value.Backing.Type)
		fmt.Printf(strings.Repeat(indent, 3)+"Label: %v\n", v.Value.Label)
		fmt.Printf(strings.Repeat(indent, 3)+"StartConnected: %v\n", v.Value.StartConnected)
		fmt.Printf(strings.Repeat(indent, 3)+"State: %v\n", v.Value.State)
	}
	fmt.Printf("GuestOS: %v\n", vmi.GuestOS)
	fmt.Println("Hardware:")
	fmt.Printf(indent+"UpgradePolicy: %v\n", vmi.Hardware.UpgradePolicy)
	fmt.Printf(indent+"UpgradeStatus: %v\n", vmi.Hardware.UpgradeStatus)
	fmt.Printf(indent+"UpgradeVersion: %v\n", vmi.Hardware.UpgradeVersion)
	fmt.Printf(indent+"Version: %v\n", vmi.Hardware.Version)
	fmt.Println("Identity:")
	fmt.Printf(indent+"BiosUUID: %v\n", vmi.Identity.BiosUUID)
	fmt.Printf(indent+"InstanceUUID: %v\n", vmi.Identity.InstanceUUID)
	fmt.Printf(indent+"Name: %v\n", vmi.Identity.Name)
	fmt.Printf("InstantCloneFrozen: %v\n", vmi.InstantCloneFrozen)
	fmt.Println("Memory:")
	fmt.Printf(indent+"HotAddEnabled: %v\n", vmi.Memory.HotAddEnabled)
	fmt.Printf(indent+"HotAddIncrementSizeMiB: %v\n", vmi.Memory.HotAddIncrementSizeMiB)
	fmt.Printf(indent+"HotAddLimitMiB: %v\n", vmi.Memory.HotAddLimitMiB)
	fmt.Printf(indent+"SizeMiB: %v\n", vmi.Memory.SizeMiB)
	fmt.Printf("Name: %v\n", vmi.Name)
	fmt.Println("Nics:")
	for i, v := range vmi.Nics {
		fmt.Printf(indent+"%v:\n", i+1)
		fmt.Printf(strings.Repeat(indent, 2)+"Key: %v\n", v.Key)
		fmt.Println(strings.Repeat(indent, 2) + "Value:")
		fmt.Printf(strings.Repeat(indent, 3)+"AllowGuestControl: %v\n", v.Value.AllowGuestControl)
		fmt.Println(strings.Repeat(indent, 3) + "Backing:")
		fmt.Printf(strings.Repeat(indent, 4)+"ConnectionCookie: %v\n", v.Value.Backing.ConnectionCookie)
		fmt.Printf(strings.Repeat(indent, 4)+"DistributedPort: %v\n", v.Value.Backing.DistributedPort)
		fmt.Printf(strings.Repeat(indent, 4)+"DistributedSwitchUUID: %v\n", v.Value.Backing.DistributedSwitchUUID)
		fmt.Printf(strings.Repeat(indent, 4)+"HostDevice: %v\n", v.Value.Backing.HostDevice)
		fmt.Printf(strings.Repeat(indent, 4)+"Network: %v\n", v.Value.Backing.Network)
		fmt.Printf(strings.Repeat(indent, 4)+"NetworkName: %v\n", v.Value.Backing.NetworkName)
		fmt.Printf(strings.Repeat(indent, 4)+"OpaqueNetworkID: %v\n", v.Value.Backing.OpaqueNetworkID)
		fmt.Printf(strings.Repeat(indent, 4)+"OpaqueNetworkType: %v\n", v.Value.Backing.OpaqueNetworkType)
		fmt.Printf(strings.Repeat(indent, 4)+"Type: %v\n", v.Value.Backing.Type)
		fmt.Printf(strings.Repeat(indent, 3)+"Label: %v\n", v.Value.Label)
		fmt.Printf(strings.Repeat(indent, 3)+"MacAddress: %v\n", v.Value.MacAddress)
		fmt.Printf(strings.Repeat(indent, 3)+"MacType: %v\n", v.Value.MacType)
		fmt.Printf(strings.Repeat(indent, 3)+"PciSlotNumber: %v\n", v.Value.PciSlotNumber)
		fmt.Printf(strings.Repeat(indent, 3)+"StartConnected: %v\n", v.Value.StartConnected)
		fmt.Printf(strings.Repeat(indent, 3)+"State: %v\n", v.Value.State)
		fmt.Printf(strings.Repeat(indent, 3)+"Type: %v\n", v.Value.Type)
		fmt.Printf(strings.Repeat(indent, 3)+"UptCompatibilityEnabled: %v\n", v.Value.UptCompatibilityEnabled)
		fmt.Printf(strings.Repeat(indent, 3)+"WakeOnLanEnabled: %v\n", v.Value.WakeOnLanEnabled)
	}
	fmt.Println("ParallelPorts:")
	for i, v := range vmi.ParallelPorts {
		fmt.Printf(indent+"%v:\n", i+1)
		fmt.Printf(strings.Repeat(indent, 2)+"Key: %v\n", v.Key)
		fmt.Println(strings.Repeat(indent, 2) + "Value:")
		fmt.Printf(strings.Repeat(indent, 3)+"AllowGuestControl: %v\n", v.Value.AllowGuestControl)
		fmt.Println(strings.Repeat(indent, 3) + "Backing:")
		fmt.Printf(strings.Repeat(indent, 4)+"AutoDetect: %v\n", v.Value.Backing.AutoDetect)
		fmt.Printf(strings.Repeat(indent, 4)+"File: %v\n", v.Value.Backing.File)
		fmt.Printf(strings.Repeat(indent, 4)+"HostDevice: %v\n", v.Value.Backing.HostDevice)
		fmt.Printf(strings.Repeat(indent, 4)+"Type: %v\n", v.Value.Backing.Type)
		fmt.Printf(strings.Repeat(indent, 3)+"Label: %v\n", v.Value.Label)
		fmt.Printf(strings.Repeat(indent, 3)+"StartConnected: %v\n", v.Value.StartConnected)
		fmt.Printf(strings.Repeat(indent, 3)+"State: %v\n", v.Value.State)
	}
	fmt.Printf("PowerState: %v\n", vmi.PowerState)
	fmt.Println("SataAdapters:")
	for i, v := range vmi.SataAdapters {
		fmt.Printf(indent+"%v:\n", i+1)
		fmt.Printf(strings.Repeat(indent, 2)+"Key: %v\n", v.Key)
		fmt.Println(strings.Repeat(indent, 2) + "Value:")
		fmt.Printf(strings.Repeat(indent, 3)+"Bus: %v\n", v.Value.Bus)
		fmt.Printf(strings.Repeat(indent, 3)+"Label: %v\n", v.Value.Label)
		fmt.Printf(strings.Repeat(indent, 3)+"PciSlotNumber: %v\n", v.Value.PciSlotNumber)
		fmt.Printf(strings.Repeat(indent, 3)+"Type: %v\n", v.Value.Type)
	}
	fmt.Println("ScsiAdapters:")
	for i, v := range vmi.ScsiAdapters {
		fmt.Printf(indent+"%v:\n", i+1)
		fmt.Printf(strings.Repeat(indent, 2)+"Key: %v\n", v.Key)
		fmt.Println(strings.Repeat(indent, 2) + "Value:")
		fmt.Printf(strings.Repeat(indent, 3)+"Label: %v\n", v.Value.Label)
		fmt.Printf(strings.Repeat(indent, 3)+"PciSlotNumber: %v\n", v.Value.PciSlotNumber)
		fmt.Println(strings.Repeat(indent, 3) + "Scsi:")
		fmt.Printf(strings.Repeat(indent, 4)+"Bus: %v\n", v.Value.Scsi.Bus)
		fmt.Printf(strings.Repeat(indent, 4)+"Unit: %v\n", v.Value.Scsi.Unit)
		fmt.Printf(strings.Repeat(indent, 3)+"Sharing: %v\n", v.Value.Sharing)
		fmt.Printf(strings.Repeat(indent, 3)+"Type: %v\n", v.Value.Type)
	}
	fmt.Println("SerialPorts:")
	for i, v := range vmi.SerialPorts {
		fmt.Printf(indent+"%v:\n", i+1)
		fmt.Printf(strings.Repeat(indent, 2)+"Key: %v\n", v.Key)
		fmt.Println(strings.Repeat(indent, 2) + "Value:")
		fmt.Printf(strings.Repeat(indent, 3)+"AllowGuestControl: %v\n", v.Value.AllowGuestControl)
		fmt.Println(strings.Repeat(indent, 3) + "Backing:")
		fmt.Printf(strings.Repeat(indent, 4)+"AutoDetect: %v\n", v.Value.Backing.AutoDetect)
		fmt.Printf(strings.Repeat(indent, 4)+"File: %v\n", v.Value.Backing.File)
		fmt.Printf(strings.Repeat(indent, 4)+"HostDevice: %v\n", v.Value.Backing.HostDevice)
		fmt.Printf(strings.Repeat(indent, 4)+"NetworkLocation: %v\n", v.Value.Backing.NetworkLocation)
		fmt.Printf(strings.Repeat(indent, 4)+"NoRxLoss: %v\n", v.Value.Backing.NoRxLoss)
		fmt.Printf(strings.Repeat(indent, 4)+"Pipe: %v\n", v.Value.Backing.Pipe)
		fmt.Printf(strings.Repeat(indent, 4)+"Proxy: %v\n", v.Value.Backing.Proxy)
		fmt.Printf(strings.Repeat(indent, 4)+"Type: %v\n", v.Value.Backing.Type)
		fmt.Printf(strings.Repeat(indent, 3)+"Label: %v\n", v.Value.Label)
		fmt.Printf(strings.Repeat(indent, 3)+"StartConnected: %v\n", v.Value.StartConnected)
		fmt.Printf(strings.Repeat(indent, 3)+"State: %v\n", v.Value.State)
		fmt.Printf(strings.Repeat(indent, 3)+"YieldOnPoll: %v\n", v.Value.YieldOnPoll)
	}
}
